package org.apache.tika.parser.microsoft;

import java.io.IOException;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.chalktips.wovenparser.Weaver;

public aspect WordExtractorRouter {

	// ~ Private Properties ==========================================

	final Logger log = LoggerFactory.getLogger(WordExtractorRouter.class.getCanonicalName());

	// ~ Pointcuts ===================================================

	pointcut parent(): withincode(* OfficeParser.parse(..));

	pointcut constructor(): parent() && call(WordExtractor.new(..));

	pointcut parse(WordExtractor we, POIFSFileSystem fs, XHTMLContentHandler xhtml):
		call(protected void WordExtractor.parse(POIFSFileSystem, XHTMLContentHandler)) &&
		target(we) &&
		args(fs, xhtml);

	// ~ Advices =====================================================

	before(): constructor() {
		log.debug("Calling Constructor...");
	}

	before(WordExtractor we, POIFSFileSystem fs, XHTMLContentHandler xhtml): parse(we, fs, xhtml) {
		log.debug("Starting parse...");
	}

	void around(WordExtractor we, POIFSFileSystem fs, XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException: parse(we, fs, xhtml) {
		Weaver.weave(fs, xhtml);
	}

}
