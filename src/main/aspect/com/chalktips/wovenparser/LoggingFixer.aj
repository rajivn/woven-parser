package com.chalktips.wovenparser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;

/**
 * This aspect attempts to fix the mess made by edu.ucar.netcdf. Their package
 * embeds the slf4j-jdk14 logging package. Since this package has to be on the
 * classpath to meet Tika dependencies, we are bound to the JDK14 logging
 * implementation. This aspect, intercepts any log.debug and log.info call and
 * routes it to the log4j implementation if it is found on the classpath. Else
 * no message will be printed.
 * 
 * @author rajiv
 */
public aspect LoggingFixer {

	// ~ Private Properties ==========================================

	protected static final String fullyQualifiedLoggerClassName = "org.apache.log4j.Logger";
	private static Class loggerClass = null;
	private static Object logger = null;
	private static Method debug = null;
	private static Method info = null;

	static {
		try {

			ClassLoader loader = LoggingFixer.class.getClassLoader();
			loggerClass = loader.loadClass(fullyQualifiedLoggerClassName);

			Method getLogger = loggerClass.getMethod("getLogger", new Class[] { Class.class });
			logger = getLogger.invoke(loggerClass, new Object[] { LoggingFixer.class });

			Method isDebugEnabled = loggerClass.getMethod("isDebugEnabled", null);
			Object debugObj = invokeMethodOnLogger(isDebugEnabled, null);
			Boolean enabled = (Boolean) debugObj;
			if (enabled.booleanValue()) {
				debug = loggerClass.getMethod("debug", new Class[] { Object.class });
			}

			Method isInfoEnabled = loggerClass.getMethod("isInfoEnabled", null);
			Object infoObj = invokeMethodOnLogger(isInfoEnabled, null);
			Boolean infoEnabled = (Boolean) infoObj;
			if (infoEnabled.booleanValue()) {
				info = loggerClass.getMethod("info", new Class[] { Object.class });
			}

		} catch (ClassNotFoundException e) {
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		} catch (Exception e) {
		}
	}

	// ~ Public Methods ==============================================

	public static Object invokeMethodOnLogger(Method m, Object[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Object ret = null;
		ret = m.invoke(loggerClass.cast(logger), args);
		return ret;
	}

	pointcut debugcall(Logger l, String s):
		!within(LoggingFixer) &&
		target(l) &&
		args(s) &&
		call(void debug(String));

	void around(Logger l, String s): debugcall(l, s) {
		if (debug != null) {
			try {
				invokeMethodOnLogger(debug, new Object[] { s });
			} catch (Exception e) {
				proceed(l, s);
			}
		} else {
			proceed(l, s);
		}
	}

	pointcut infocall(Logger l, String s):
		!within(LoggingFixer) &&
		target(l) &&
		args(s) &&
		call(void info(String));

	void around(Logger l, String s): infocall(l, s) {
		if (info != null) {
			try {
				invokeMethodOnLogger(info, new Object[] { s });
			} catch (Exception e) {
				proceed(l, s);
			}
		} else {
			proceed(l, s);
		}
	}

}
