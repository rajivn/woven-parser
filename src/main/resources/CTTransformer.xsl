<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:h="http://www.w3.org/1999/xhtml"
	xmlns:utils="com.chalktips.wovenparser.Weaver" exclude-result-prefixes="html utils h">

	<xsl:strip-space elements="*" />
	<xsl:output method="html" encoding="utf-8"
		omit-xml-declaration="yes" />

	<xsl:template match="h:head">
		<head>
			<xsl:for-each select="child::*">
				<xsl:call-template name="default" />
			</xsl:for-each>
			<style>
				<xsl:value-of select="../h:body/h:style" />
			</style>
		</head>
	</xsl:template>

	<xsl:template match="h:body">
		<body>
			<xsl:for-each select="child::*">
				<xsl:choose>
					<xsl:when test="local-name() != 'style'">
						<xsl:call-template name="default" />
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</body>
	</xsl:template>

	<xsl:template match="@src">
		<xsl:attribute name="src">
			<xsl:value-of select="utils:removeCRLF(.)" />
		</xsl:attribute>
	</xsl:template>

	<xsl:template name="default" match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/">
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html>
</xsl:text>
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>
