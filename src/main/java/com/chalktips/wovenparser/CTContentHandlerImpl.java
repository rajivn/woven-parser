package com.chalktips.wovenparser;

import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.chalktips.parser.domain.CTContentHandler;

public class CTContentHandlerImpl implements CTContentHandler {

	// ~ Private Properties ==========================================

	private XHTMLContentHandler handler = null;

	// ~ Constructors ================================================

	public CTContentHandlerImpl(XHTMLContentHandler handler) {
		this.handler = handler;
	}

	// ~ Public Methods ==============================================

	public void element(String name, String value) throws SAXException {
		handler.element(name, value);
	}

	public void startElement(String name, AttributesImpl attributes) throws SAXException {
		handler.startElement(name, attributes);
	}

	public void endElement(String name) throws SAXException {
		handler.endElement(name);
	}

	public void characters(String characters) throws SAXException {
		handler.characters(characters);
	}

}
