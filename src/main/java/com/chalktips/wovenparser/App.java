package com.chalktips.wovenparser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.TransformerException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class App {

	// ~ Private Properties ==========================================

	private static final Parser parser = new AutoDetectParser();

	// ~ Public Methods ==============================================

	/**
	 * Main method. Accepts one filename as argument and outputs to
	 * <code>System.out</code> the HTML generated from parser.
	 * 
	 * @param args
	 *            - Name of the file.
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("No file name specified. ...Exiting");
			System.exit(1);
		}

		int exitStatus = 0;
		Metadata metadata = null;
		ParseContext context = null;
		InputStream stream = null;

		try {

			File file = new File(args[0]);
			if (file.isFile()) {
				stream = new FileInputStream(file);
			} else {
				stream = App.class.getClassLoader().getResourceAsStream(args[0]);
			}
			if (stream == null) {
				System.err.println("Could not find file: " + args[0]);
				System.exit(1);
			}

			metadata = new Metadata();
			// Just give the document a default title.
			metadata.set(Metadata.TITLE, "Chalktips Document");

			context = new ParseContext();
			context.set(Parser.class, parser);

			File tmpFile = File.createTempFile("wp-", ".xml");
			FileOutputStream outStream = new FileOutputStream(tmpFile);

			ContentHandler handler = Weaver.getContentHandler(outStream);
			parser.parse(stream, handler, metadata, context);
			outStream.flush();
			outStream.close();

			FileInputStream inputStream = new FileInputStream(tmpFile);
			Weaver.transformToHTML(inputStream, System.out);
			inputStream.close();

			tmpFile.delete();

		} catch (IOException e) {

			System.out.println("Exception was thrown.");
			e.printStackTrace();
			exitStatus = 1;

		} catch (SAXException e) {

			System.out.println("Exception was thrown.");
			e.printStackTrace();
			exitStatus = 1;

		} catch (TikaException e) {

			System.out.println("Exception was thrown.");
			e.printStackTrace();
			exitStatus = 1;

		} catch (TransformerException e) {

			System.out.println("Exception was thrown.");
			e.printStackTrace();
			exitStatus = 1;

		} finally {

			try {

				context = null;
				metadata = null;
				if (stream != null) {
					stream.close();
				}
				stream = null;

			} catch (IOException e) {
			}

			System.exit(exitStatus);

		}
	}
}
