package com.chalktips.wovenparser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import com.chalktips.parser.domain.CTContentHandler;
import com.chalktips.parser.domain.CTDocument;
import com.chalktips.parser.impl.DocParser;

/**
 * Class used to aid weaving of {@link com.chalktips.parser.Parser Chalktips
 * Parser} with {@link org.apache.tika.parser.microsoft.OfficeParser
 * OfficeParser}
 * 
 * @author rajiv
 */
public class Weaver {

	// ~ Private Properties ==========================================

	private static final String XSL_FILE_NAME = "CTTransformer.xsl";

	private static SAXTransformerFactory factory = null;
	private static Transformer finalizer = null;

	static {

		try {

			InputStream xslInputStream =
						Weaver.class.getClassLoader().getResourceAsStream(XSL_FILE_NAME);
			factory = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
			Source xslSource = new StreamSource(xslInputStream);

			Templates xslt = factory.newTemplates(xslSource);
			finalizer = factory.newTransformerHandler(xslt).getTransformer();

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}

		finalizer.setOutputProperty(OutputKeys.METHOD, "html");
		finalizer.setOutputProperty(OutputKeys.INDENT, "yes");
		finalizer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		finalizer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	}

	// ~ Public Methods ==============================================

	public static void weave(final POIFSFileSystem fs, final XHTMLContentHandler xhtml)
			throws IOException, SAXException, TikaException {

		DocParser parser = new DocParser(fs);
		CTDocument doc = parser.parse();
		if (doc == null) {
			return;
		}

		CTContentHandler ctHandler = new CTContentHandlerImpl(xhtml);
		doc.appendToHandler(ctHandler);
		ctHandler = null;
	}

	public static void transformToHTML(InputStream is, OutputStream os) throws TransformerException {
		finalizer.transform(new StreamSource(is), new StreamResult(os));
	}

	public static String removeCRLF(String s) {
		int index = s.indexOf(",");
		String uriPrefix = "";
		String encodedString = "";
		if (index > 0) {
			uriPrefix = s.substring(0, index);
			// TODO: Review this. Don't know why this is required but the system
			// does not work without this.
			String base64String = s.substring(index + 1);
			byte[] data = Base64.decodeBase64(base64String);
			encodedString = Base64.encodeBase64String(data);
		}
		return uriPrefix + "," + encodedString;
	}

	public static ContentHandler getContentHandler(OutputStream out) throws TransformerConfigurationException {
		TransformerHandler handler = factory.newTransformerHandler();

		Transformer transformer = handler.getTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "html");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

		handler.setResult(new StreamResult(out));
		return handler;
	}
}
